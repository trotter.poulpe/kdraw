package fr.kdraw;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.Pane;
import javafx.stage.Popup;
import javafx.stage.Stage;


public class ControllerAbout {
	@FXML
	private MenuBar menuBar;
	
	@FXML
	private Pane mainPane;
	
	@FXML
	private Stage stageAbout;

    @FXML
    public void initialize() {
    	

    }
    
    @FXML
    private void okClicked(ActionEvent event) {
    	stageAbout.close();
    }
    

	@FXML
	private void urlClicked(ActionEvent event) throws IOException {
		System.out.println("Faut ouvrir le browser !");
        try {
            Desktop.getDesktop().browse(new URI(((Hyperlink)event.getSource()).getText()));
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (URISyntaxException e1) {
            e1.printStackTrace();
        }

	}
	
}
