package fr.kdraw;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;


public class ControllerMain {
	@FXML
	private MenuBar menuBar;
	
	@FXML
	private Pane mainPane;
	
	private Stage stageAbout;

    @FXML
    public void initialize() {
    	

    }

	@FXML
	private void menuAboutClicked() throws IOException {
		if(stageAbout!=null) {
			stageAbout.close();
		}
		stageAbout = FXMLLoader.load(getClass().getResource("fxml/About.fxml"));
		stageAbout.initModality(Modality.APPLICATION_MODAL); 
		stageAbout.show();

	}

	public Stage getStageAbout() {
		return stageAbout;
	}

	public void setStageAbout(Stage stageAbout) {
		this.stageAbout = stageAbout;
	}
	
}
