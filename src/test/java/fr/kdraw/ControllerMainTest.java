package fr.kdraw;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import com.sun.javafx.scene.control.skin.ContextMenuContent;
import com.sun.javafx.scene.control.skin.ContextMenuContent.MenuItemContainer;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ControllerMainTest extends ApplicationTest  {
	
	ControllerMain controllerMain;
	Parent root;
	MenuItem aboutMI;
	ContextMenuContent.MenuItemContainer mainMenu;
	
    @Override
    public void start(Stage stage) throws Exception {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/Main.fxml"));
    	root = (BorderPane)fxmlLoader.load();
    	controllerMain = (ControllerMain) fxmlLoader.getController();
    	System.out.println("root:::"+root);
    	System.out.println("CC:::"+controllerMain);

        stage.setScene(new Scene(root));
        stage.show();
        /* Do not forget to put the GUI in front of windows. Otherwise, the robots may interact with another
        window, the one in front of all the windows... */
        stage.toFront();
    }
    
    @After
    public void tearDown() throws TimeoutException {
        /* Close the window. It will be re-opened at the next test. */
        FxToolkit.hideStage();
        release(new KeyCode[] {});
        release(new MouseButton[] {});
    }
    
    @Test
    public void testClickOnAboutMenuCreatePopup() {
    	 clickOn("#help").clickOn("#about").sleep(100);
    	 assertNotNull(controllerMain.getStageAbout());
    }
    
}
